#! /usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

echo "--- install vim ---"
# text editor
sudo apt install -y vim
echo "=> Done"

echo "--- install curl ---"
# tool to make http requests
sudo apt install -y curl
echo "=> Done"

echo "--- install silversearcher(ag) ---"
# tool to search files super fast
sudo apt install -y silversearcher-ag
echo "=> Done"

echo "--- install system monitor (htop) ---"
# tool to monitor system rescources
sudo apt install -y htop
echo "=> Done"

