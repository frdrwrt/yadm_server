#! /usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

echo "--- setup git ---"
read -p "git name:" name
read -p "git email:" email

git config --global user.name $name
git config --global user.email $email

echo "=> Done"
