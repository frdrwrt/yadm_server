#! /usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

sudo apt install -y build-essential
echo "--- install node via nvm ---"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.1/install.sh | bash
set +euo
eval "$(cat ~/.bashrc)"
set -euo
nvm install --lts
echo "=> Done!"

