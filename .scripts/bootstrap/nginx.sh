#! /usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'


echo "--- install nginx ---"
sudo apt update
sudo apt install -y nginx

echo "--- update ufw for http(80)  ---"
sudo ufw allow "Nginx HTTP"
sudo ufw status

echo "--- copy example sites to /etc/nginx/sites-available/ ---"
sudo cp -R ~/.scripts/bootstrap/nginx-sites/* /etc/nginx/sites-available/
echo "=> Done!"
echo "for further configuration see https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-debian-10#step-1-%E2%80%93-installing-nginx"

