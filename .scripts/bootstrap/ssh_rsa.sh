#! /usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

echo "--- create rsa key pair ---"

# generate key
ssh-keygen

# start ssh agent
eval $(ssh-agent -s)

# add key to ssh single sign-on
ssh-add

echo "=> Done"
