#! /usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

echo "--- setup firewall (ufw and fail2ban) ---"

read -p "what is the ssh port?[default 22]" ssh_port

sudo apt -y install ufw fail2ban

# add new openssh application with custom port to ufw
sudo -- sh -c "echo '\n[OpenSSH-$ssh_port]\ntitle=Secure shell server\ndescription=OpenSSH with custom port\nports=$ssh_port/tcp' >> /etc/ufw/applications.d/openssh-server"

# restart ufw
sudo service ufw restart

# add local fail2ban config
jail_conf=/etc/fail2ban/jail.local
sudo cp /etc/fail2ban/jail.conf $jail_conf

# redirect banaction to ufw
sudo sed -i 's/banaction = iptables-multiport/banaction = ufw/g' $jail_conf

# secure ssh login on custom port and connect to ufw
sudo sed -i "s/port    = ssh/port = $ssh_port\naction = ufw[application='OpenSSH-$ssh_port', blocktype=reject]/g" $jail_conf

# reload fail2ban
sudo fail2ban-client reload

# deny all other ports by default
sudo ufw default deny

# allow custem ssh port
sudo ufw allow OpenSSH-$ssh_port

# restart ufw
sudo ufw enable
sudo ufw status verbose
echo "=> Done"
