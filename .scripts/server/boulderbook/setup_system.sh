#! /usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

bootstrap=$HOME/.scripts/bootstrap/
boulderbook=$HOME/boulderbook
echo "--- install dependencies ---"
$bootstrap/node.sh
set +euo
eval "$(cat ~/.bashrc)"
set -euo
nvm install lts/carbon
nvm use lts/carbon
nvm alias default lts/carbon
nvm uninstall --lts

$bootstrap/couchdb.sh

$bootstrap/nginx.sh

npm install -g pm2
echo "=> Done!"

echo "--- clone repo ---"
[ -d $boulderbook ] || git clone git@gitlab.com:boulderbook/boulderbook.git $boulderbook
echo "=> Done"

echo "--- setup nginx ---"
sudo ln -fs /etc/nginx/sites-available/couchdb /etc/nginx/sites-enabled/couchdb
sudo ln -fs /etc/nginx/sites-available/node-server /etc/nginx/sites-enabled/node-server

sudo systemctl restart nginx

sudo ufw allow 443
sudo ufw allow 4008

echo "=> Done"

$HOME/.scripts/server/boulderbook/update.sh
pm2 save
pm2 kill
pm2 startup

