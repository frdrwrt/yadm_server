#! /usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

boulderbook=$HOME/boulderbook

echo "--- update repo ---"
cd $boulderbook
git pull
echo "=> Done!"

echo "--- clean dist ---"
cd $boulderbook/server/
rm -rf node_modules
rm -rf dist
echo "=> Done!"

echo "--- install and build ---"
npm install
npm run build 
echo "=> Done!"

echo "--- restart server ---"
pm2 restart pm2.yml
echo "=> Done!"



