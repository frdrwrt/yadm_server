#! /usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

source ~/.environment

while true; do
	read -p "Are you sure to reset couchDB? All data will be lost!! [y]?" y
	case $y in
		[Yy]* ) break;;
		* ) exit 1
	esac
done

echo "--- start reset couchDB ---"
node ~/boulderbook/server/dist/server/couchdb/reset-couch.js localhost $COUCH_PORT
echo "=> Done!"
