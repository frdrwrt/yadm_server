#! /usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'
echo "--- start update system and packages ---"
sudo apt -y update
sudo apt -y upgrade
sudo apt -y full-upgrade
sudo apt -y autoremove
echo "=> Done"

