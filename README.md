1. connect to server
- `ssh -o PubkeyAuthentication=no root@<IP>`
use to force password authentication if you manage multiple ssh keys on your local machine

2. create new user
- `adduser <USER>`
you can choose a easy typable password because we limit access via rsa key later
- `usermod -aG sudo <USER>`

3. check if sudo is installed and user has root access
- `su <USER>`
- `sudo whomai`
if sudo is not installed run `apt install sudo`

4. add ssh key to user 
- `mkdir /home/<USER>/.ssh`
- `touch /home/<USER>/.ssh/authorized_keys`
- open file and paste your ssh rsa.pub key
- separate multiple keys by newline

5. enable ssh access for user
- open `/etc/ssh/sshd_config`
- add line at end `AllowUsers <USER>`
- `sudo systemctl restart ssh`
exit shell and try to login with `ssh <USER>@<IP>` without use of password(except your rsa pw)

6. disable password and root login
- open `/etc/ssh/sshd_config`
- set PasswordAuthentication -> no
- set PermitRootLogin -> no
- set UsePAM -> no
- add line `Port <CUSTOM_PORT>`
standard port 22 is often used attacks
- `sudo systemctl restart ssh`

No it should only be possible to access <USER> via ssh if ssh rsa key is registered. No root login. No password authentication

`ssh <USER>@<IP> -p <CUSTOM_PORT>`

7. setup yadm (dotfile manager)
- `sudo apt install -y yadm`
- `yadm clone https://gitlab.com/frdrwrt/yadm_server.git`
